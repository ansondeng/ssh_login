#-*- coding: utf-8 -*-

#!/usr/bin/python

import paramiko
import yaml
import threading




def ssh2(ip,username,passwd,cmd):

    try:

        ssh = paramiko.SSHClient()

        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())

        ssh.connect(ip,22,username,passwd,timeout=5)

        for m in cmd:

            stdin, stdout, stderr = ssh.exec_command(m)

#           stdin.write("Y")   #简单交互，输入 ‘Y’

            out = stdout.readlines()

            #屏幕输出

            for o in out:

                print o,

        print '%s\tOK\n'%(ip)

        ssh.close()

    except :

        print '%s\tError\n'%(ip)





if __name__=='__main__':

    f = open("settings.yaml")
    config = yaml.load(f)
    f.close()

    # 需要执行的命令
    #cmd = config['execs']
    cmd = []
    # 登录用户名称
    username = config['username']
    # 登录密码
    passwd = config['passwd']

    # wifi ssid和密码
    ssid = config["ssid"]
    psk = config["psk"]

    # 修改后ip
    address = config["address"]

    # 登录IP列表
    ips = config['ips']

    threads = []

    for i in range(0, len(ips)):
        ip = "192.168.1." + str(100+ips[i]);

        # 需要执行的命令
        cmd = []
        cmd.append("sudo cp /etc/dhcpcd.conf /etc/dhcpcd.conf_bk");

        if psk != None:
            tmp = "sudo sed -in \"/^wpa-psk/c wpa-psk " + str(psk) + " \" /etc/network/interfaces"
            cmd.append(tmp)
        else:
            print "psk empty"
        if address != None:
            tmp = "sudo sed -in \"/^address/c address 192.168.1." + str(100 + address[i]) + " \" /etc/network/interfaces"
            cmd.append(tmp)
        else:
            print "address empty"
        cmd.append("sudo reboot");

        print "Beging Exec:%s on server ip: %s..." % (cmd, ip)

        a = threading.Thread(target=ssh2, args=(ip, username, passwd, cmd))
        a.start()
    print "Done!"



